// Đồng bộ , bất đồng bộ

// setTimeout(function () {
//   console.log("3");
// }, 5000);

// setTimeout(function () {
//   console.log("4");
// }, 1000);
console.log("1");
console.log("2");

setTimeout(function () {
  console.log("5000");
}, 0);

//
// request response
axios({
  url: "https://63b2c99f5901da0ab36dbaed.mockapi.io/food",
  method: "GET",
})
  .then(function (res) {
    // xử lý khi gọi api thành công
    console.log(`  🚀: res`, res.data);
  })
  .catch(function (err) {
    // xứ lý khi gọi api thất bại
    console.log(`  🚀: err`, err);
  });
