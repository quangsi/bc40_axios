const BASE_URL = "https://63b2c99f5901da0ab36dbaed.mockapi.io";

// render danh sách món ăn
function renderFoodList(foods) {
  // true => mặn
  // flase => chay
  var contentHTML = "";

  foods.reverse().forEach(function (item) {
    var contentTr = ` <tr>
                        <td>${item.maMon}</td>
                        <td>${item.tenMon}</td>
                        <td>${item.giaMon}</td>
                        <td>${
                          item.loaiMon
                            ? "<span class='text-primary'>Mặn</span>"
                            : " <span class='text-success'>Chay</span>"
                        }</td>
                        <td>
                        ${convertString(50, item.hinhAnh)}
                        </td>
                        <td>
                        <button onclick="xoaMonAn('${
                          item.maMon
                        }')" class="btn btn-danger">Xoá</button>
                        <button onclick="suaMonAn('${
                          item.maMon
                        }')" class="btn btn-warning">Sửa</button>
                        </td>
                      </tr>`;
    contentHTML += contentTr;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
}

// gọi api lấy danh sách món ăn từ server
function fetchFoodList() {
  // bật loading 1 lần trước khi api được gọi
  // tắt loading 2 lần trong then và catch
  batLoading();
  axios({
    url: `${BASE_URL}/food`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      var foodList = res.data;
      renderFoodList(foodList);
    })
    .catch(function (err) {
      tatLoading();
      console.log(`  🚀: err`, err);
    });
}
// khi user load trang
fetchFoodList();

// xoá món ăn

function xoaMonAn(id) {
  console.log(`  🚀: xoaMonAn -> id`, id);
  batLoading();
  axios({
    url: `${BASE_URL}/food/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      tatLoading();
      // gọi lại api lấy danh sách  sau khi xoá thành công
      fetchFoodList();
    })
    .catch(function (err) {
      tatLoading();
      console.log(`  🚀: xoaMonAn -> err`, err);
    });
}

// thêm món ăn

function themMonAn() {
  var monAn = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/food`,
    method: "POST",
    data: monAn,
  })
    .then(function (res) {
      fetchFoodList();
    })
    .catch(function (err) {
      console.log(`  🚀: themMonAn -> err`, err);
    });
}
// pending, resolve ( success ), reject ( fail )

function suaMonAn(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/food/${id}`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      //
      // document.getElementById("maMon").disabled = true;
      console.log(`  🚀: suaMonAn -> res`, res);
    })
    .catch(function (err) {
      tatLoading();
      console.log(`  🚀: suaMonAn -> err`, err);
    });
}

function capNhatMonAn() {
  // layThongTinTuForm
  var monAn = layThongTinTuForm();

  axios({
    url: `${BASE_URL}/food/${monAn.maMon}`,
    method: "PUT",
    data: monAn,
  });
}
// promise all , promise chaining
